import {Item} from './item';
export const ITEMS:Item[]=[
    {id:1,name:'Aalu Bhugia',price:10,quantity:100,active:true,type:'Snacks'},
    {id:2,name:'Mixture',price:15,quantity:100,active:true,type:'Snacks'},
    {id:3,name:'Navratna',price:20,active:true,quantity:100,type:'Snacks'},
    {id:4,name:'gulab Jamun',price:100,quantity:1000,active:true,type:'Sweets'}
]