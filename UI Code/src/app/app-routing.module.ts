import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule,Routes} from '@angular/router';
import {ItemsComponent} from './items/items.component';
import{StoreComponent} from'./store/store.component';
import{ItemDetailComponent} from './item-detail/item-detail.component';
import{} from '';
const routes:Routes=[
  {path:'item',component:ItemsComponent},
  {path:'home',component:StoreComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path:'details/:id',component:ItemDetailComponent}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
  
})
//   imports: [
//     CommonModule
//   ],
//   declarations: []
// })
export class AppRoutingModule { }
