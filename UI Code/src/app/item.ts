export class Item{
    public id: number;
    public name:string;
    public price:number;
    public quantity:number;
    public type:string;
    public active:boolean;
}