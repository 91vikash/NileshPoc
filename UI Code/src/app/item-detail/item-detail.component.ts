import { Component, OnInit,Input } from '@angular/core';
import{Item} from '../item';
import{ActivatedRoute} from'@angular/router';
import{Location}from'@angular/common';
import{ItemService} from'../item.service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
@Input() item:Item
  constructor(private itemService:ItemService, private route: ActivatedRoute,private location:Location
  ) { }
  getItemDetails():void{
    debugger;
    const id=+this.route.snapshot.paramMap.get('id');
    this.itemService.getItemdetails(id).subscribe(item=>this.item=item);
  }

  ngOnInit():void {
    this.getItemDetails();
  }
  goBack():void{
this.location.back();
  }
  
  save():void{
    debugger;
    this.itemService.updateDetails(this.item).subscribe(()=>this.goBack());
  }

}
