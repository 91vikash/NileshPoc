import { Component, OnInit } from '@angular/core';
import {ITEMS} from '../mocked-items';
import {Item} from '../item';
import{ItemService} from '../item.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  constructor(private itemService:ItemService ) { }
items:Item[];
add56:any;
selectedItem:Item;
  ngOnInit() { this.getItems();
  }
  onSelect(item:Item):void{
this.selectedItem=item;
  }
debugger;
  add(name:string,price:number,quantity:number,type:string,active:boolean):void{
    debugger;
name=name.trim();type=type.trim();if(!name && !type){return;}
debugger;
this.itemService.addItem({name,price,quantity,type,active} as Item).subscribe(item=>{this.items.push(item);})
this.add56=Item;
  }

  getItems():void{
    this.itemService.getItems().subscribe(items=>this.items=items);
  }
  
  delete(item:Item):void{
    debugger;
    this.items=this.items.filter(i=>i !==item);
    this.itemService.deleteItem(item).subscribe();
  } 

}
