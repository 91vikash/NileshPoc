import { Injectable } from '@angular/core';
//import{ITEMS} from './mocked-items';
import {Item} from './item';
import {observable,of, Observable} from 'rxjs';
import {MessageService} from'./message.service';
import{HttpClient,HttpHeaders} from'@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators'; 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}; 
@Injectable({
  providedIn: 'root'
})
export class ItemService {

constructor(private messageService:MessageService,private http:HttpClient) { }
  
 private ItemUrl='http://localhost:36758/api/Items';

//************** Get all Items List ********************
  getItems():Observable<Item[]>{
    this.messageService.add('ItemService: fetched Items');
    //return of(ITEMS);
    return this.http.get<Item[]>(this.ItemUrl).pipe(
      tap(items => this.log('fetched Items')),
      catchError(this.handelError('',[])));
  }
//**************Get Iem for a specific Id *********************
  getItemdetails(id:number):Observable<Item>{
    const url = `${this.ItemUrl}/${id}`;
    return this.http.get<Item>(url).pipe(
      tap(_=>this.log(`item fetched with id=${id}`)),
      catchError(this.handelError<Item>('get Item :id=${id}')));
    //this.messageService.add(`ItemService: fetched Item id=${id}`);
    //return of(ITEMS.find(item=>item.id===id));
  }

//***************Update Item takes the body and the id ***********
updateDetails(item:Item):Observable<any>{
  debugger;
  const url=`${this.ItemUrl}/${item.id}`;
  return this.http.put(url,item,httpOptions).pipe(
    tap(_=>this.log(`Updated item eith id=${item.id}`)),
    catchError(this.handelError<any>('Updated Item')));
}
//**************Create a new item record ************************
addItem(item:Item):Observable<any>{
  return this.http.post<Item>(this.ItemUrl,item,httpOptions).pipe(
    tap((item:Item)=>this.log(`added new item w/ id=${item.id}`)),
    catchError(this.handelError<any>("New Item added"))
);
}

//*************Delete a item at a time ***********************
deleteItem(item:Item|number):Observable<Item>{
const id= typeof item==='number'? item:item.id;
debugger;
const url=`${this.ItemUrl}/${id}`;
return this.http.delete<Item>(url, httpOptions).pipe(
  tap(_ => this.log(`deleted item id=${id}`)),
  catchError(this.handelError<Item>('delete Item'))
);
}

//************Other Private Methodsused in the service */
  private handelError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
  };
  }
  private log(message:string){
    this.messageService.add(`ItemService: ${message}`)
  }
}
